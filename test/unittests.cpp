#include <cassert>
#include <cstdlib>

#include "palindrome.h"

using namespace palindrome;

int main(int argc, char **argv) {
    assert(isPalindrome("ana"));
    assert(isPalindrome("anna"));

    assert(!isPalindrome("anda"));
    assert(!isPalindrome("bnna"));

    return EXIT_SUCCESS;
}
